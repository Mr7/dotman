" This is ofshellohicy's .vimrc file

call pathogen#infect()
call pathogen#helptags()

filetype off

" Use Vim settings, rather then Vi settings (much better!).
" This must be first, because it changes other options as a side effect.
set nocompatible

" Allow backgrounding buffers without writing them, and remember marks/undo
" for backgrounded buffers
set hidden

" Remember more commands and search history
set history=1000

" Make tab completion for files/buffers act like bash
set wildmenu

" Make searches case-sensitive only if they contain upper-case characters
set ignorecase
set smartcase

" Keep more context when scrolling off the end of a buffer
set scrolloff=3

" Store temporary files in a central spot
set backupdir=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set directory=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp

" allow backspacing over everything in insert mode
set backspace=indent,eol,start

" show the cursor position all the time
set ruler

" display incomplete commands
set showcmd

" syntax on
syntax on

if !has("gui_running")
  set t_Co=256
endif

set autoindent " always set autoindenting on

" Enable file type detection.
" Use the default filetype settings, so that mail gets 'tw' set to 72,
" 'cindent' is on in C files, etc.
" Also load indent files, to automatically do language-dependent indenting.
filetype plugin indent on

autocmd FileType * setl foldmethod=manual
autocmd BufRead,BufNewFile *.md setfiletype markdown
autocmd BufRead,BufNewFile *.thor setfiletype ruby
autocmd FileType css setl isk+=-

" Set the damn file type
nnoremap <LocalLeader><LocalLeader> :setfiletype<space>
nnoremap <LocalLeader>mk :setfiletype<space>html.mako<cr>
nnoremap <LocalLeader>js :setfiletype<space>javascript<cr>
let g:javascript_ignore_javaScriptdoc = 1

autocmd BufReadPost * silent! normal g`"

set autoindent
set shiftround
set expandtab smarttab
set softtabstop=4
set tabstop=4
set cindent shiftwidth=4

" set incsearch
set hlsearch
nnoremap <backspace> :nohlsearch<enter>

set t_Co=256 " 256 colors
color grb256

" GRB: Always source python.vim for Python files
au FileType python source ~/.vim/scripts/python.vim

" GRB: Use custom python.vim syntax file
au! Syntax python source ~/.vim/syntax/python.vim
let python_highlight_all = 1
let python_slow_sync = 1

" GRB: use emacs-style tab completion when selecting files, etc
set wildmode=longest,list

" GRB: Put useful info in status line
:set statusline=%<%f\ (%{&ft})%=%-19(%3l,%02c%03V%)

function! GreenBar()
    hi GreenBar ctermfg=white ctermbg=green guibg=green
    echohl GreenBar
    echon repeat(" ",&columns - 1)
    echohl
endfunction

let mapleader=","

set cursorline

" highlight current column
set cursorcolumn
hi CursorColumn cterm=NONE ctermbg=darkred ctermfg=white guibg=darkred guifg=white

set cmdheight=2

set guioptions=aeicM

augroup myfiletypes
  "clear old autocmds in group
  autocmd!
  "for ruby, autoindent with two spaces, always expand tabs
  autocmd FileType ruby,haml,eruby,yaml,html,javascript,sass set ai sw=2 sts=2 et
  autocmd FileType python set sw=4 sts=4 et
augroup END

set switchbuf=useopen

" Map ,e to open files in the same directory as the current file
cnoremap %% <C-R>=expand('%:h').'/'<cr>
map <leader>e :edit %%


"set relativenumber
set nu

" Seriously, guys. It's not like :W is bound to anything anyway.
command! W :w
command! KillWhitespace :normal :%s/ *$//g<cr><c-o><cr>

augroup mkd
    autocmd BufRead *.md  set ai formatoptions=tcroqn2 comments=n:&gt;
    autocmd BufRead *.mkd  set ai formatoptions=tcroqn2 comments=n:&gt;
    autocmd BufRead *.markdown  set ai formatoptions=tcroqn2 comments=n:&gt;
augroup END


map <silent> <leader>y :<C-u>silent '<,'>w !pbcopy<CR>

" Make <leader>' switch between ' and "
nnoremap <leader>' ""yls<c-r>={'"': "'", "'": '"'}[@"]<cr><esc>



nnoremap <c-j> <c-w>j
nnoremap <c-k> <c-w>k
nnoremap <c-h> <c-w>h
nnoremap <c-l> <c-w>l

" switch bewteen file opened by :e
nnoremap <c-n> :bnext<enter>
nnoremap <c-p> :bprevious<enter>
nnoremap <leader><leader> <c-^>

command! -range Md5 :echo system('echo '.shellescape(join(getline(<line1>, <line2>), '\n')) . '| md5')

imap <c-l> <space>=><space>


" In these functions, we don't use the count argument, but the map referencing
" v:count seems to make it work. I don't know why.
function! ScrollOtherWindowDown(count)
  normal! 
  normal! 
  normal! 
endfunction
function! ScrollOtherWindowUp(count)
  normal! 
  normal! 
  normal! 
endfunction
nnoremap g<c-y> :call ScrollOtherWindowUp(v:count)<cr>
nnoremap g<c-e> :call ScrollOtherWindowDown(v:count)<cr>

set list
set encoding=utf-8
set listchars=tab:>-,trail:~

set colorcolumn=+1        " highlight column after 'textwidth'
set colorcolumn=+1,+2,+3  " highlight three columns after 'textwidth'
highlight ColorColumn ctermbg=red  guibg=#00875f
set colorcolumn=80

"Paste toggle - when pasting something in, don't indent.
set pastetoggle=<F3>


"配置taglist
"let Tlist_Ctags_Cmd="/usr/local/bin/ctags"
let Tlist_Sort_Type="name"
let Tlist_Exit_OnlyWindow=1
let Tlist_File_Fold_Auto_Close=1
let Tlist_Auto_Open=0
map <F2> :Tlist<CR>

let g:winManagerWindowLayout='FileExplorer'
map <F4> :WMToggle<CR>

vmap <C-c> y:call system("pbcopy", getreg("\""))<CR>
nmap <C-v> :call setreg("\"",system("pbpaste"))<CR>p

let tlist_objc_settings = 'objc;i:interface;c:class;m:method;p:property;f:function'

" Remap the tab key to do autocompletion or indentation depending on the
" context (from http://www.vim.org/tips/tip.php?tip_id=102)
function! InsertTabWrapper()
    let col = col('.') - 1
    if !col || getline('.')[col - 1] !~ '\k'
        return "\<tab>"
    else
        return "\<c-p>"
    endif
endfunction
inoremap <tab> <c-r>=InsertTabWrapper()<cr>
inoremap <s-tab> <c-n>

let g:CommandTMaxHeight = 10
let g:CommandTBackspaceMap = ['<BS>', '<C-h>']
let g:CommandTCursorLeftMap = '<Left>'
nnoremap <leader>ft :CommandTFlush<enter>

hi Pmenu      ctermfg=Black    ctermbg=Gray cterm=None guifg=Brown guibg=Brown
hi PmenuSel   ctermfg=White   ctermbg=Brown cterm=Bold guifg=White guibg=Brown gui=Bold
hi PmenuSbar                  ctermbg=Brown            guibg=Brown
hi PmenuThumb ctermfg=White                           guifg=White
set previewheight=8

" use the_silver_searcher, better than ack
let g:ackprg = 'ag --nogroup --nocolor --column'

" disable auto breakline 
set textwidth=0
