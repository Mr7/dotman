! /bin/bash
# File name : checkImageSize.sh
# Author: Tang Qiao
# 

usage() {
    cat <<EOF
    Usage:
        checkImageSize.sh <directory>
EOF
}

if [ $# -ne 1 ]; then
    usage
    exit 1
fi

SRC_DIR=$1

# check src dir
if [ ! -d $SRC_DIR ]; then
    echo "src directory not exist: $SRC_DIR"
    exit 1
fi

for src_file in $SRC_DIR/*.png ; do
    echo "process file name: $src_file"
    width=`identify -format "%[fx:w]" $src_file`
    height=`identify -format "%[fx:h]" $src_file`
    # check width
    modValue=`awk -v a=$width 'BEGIN{printf "%d", a % 2}'`
    if [ "$modValue" == "1" ]; then
       echo "[Error], the file $src_file width is $width"
    fi
    # check height
    modValue=`awk -v a=$height 'BEGIN{printf "%d", a % 2}'`
    if [ "$modValue" == "1" ]; then
       echo "[Error], the file $src_file height is $height"
    fi
done
